//
//  CreatorTableViewCell.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-05.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class CreatorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
