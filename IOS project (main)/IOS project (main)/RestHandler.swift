//
//  RestHandler.swift
//  APIDemo
//
//  Created by Ebbas on 2018-11-22.
//  Copyright © 2018 enappstudio. All rights reserved.
//

import Foundation

class RestHandler {
    
    static private let API_PATH: String = "https://api.predic8.de/shop/products/?page=1&limit=100"
    
    static func getFruits(completion: ((Bool) -> Void)?) {
        guard let url = URL(string: API_PATH) else {
            completion?(false)
            return
        }
        
        let request = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            let decoder = JSONDecoder()
            if let data = responseData, let result = try? decoder.decode(FruitResponse.self, from: data) {
                DataHandler.instance.Fruit = result.products.sorted { $0.name < $1.name }
                completion?(true)
            } else {
                completion?(false)
            }
        }
        task.resume()
    }
}
