//
//  colors.swift
//  IOS project (main)
//
//  Created by Martin Malmström on 2018-11-08.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

extension UIColor {
    struct MilkshakerColors {
        static let blue = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        static let darkPink = #colorLiteral(red: 0.8084723122, green: 0.2923532209, blue: 0.7755165756, alpha: 1)
        static let lightPink = #colorLiteral(red: 0.9517487047, green: 0.6383184451, blue: 0.9174201093, alpha: 1)
    }
}
