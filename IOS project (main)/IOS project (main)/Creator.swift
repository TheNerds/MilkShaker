//
//  creator.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-05.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation


class Creator {
    let name:String
    let age:Int
    let info:String
    init(name:String,age:Int,info:String){
        self.name = name.capitalized
        self.age = age
        self.info = info
    }
}
