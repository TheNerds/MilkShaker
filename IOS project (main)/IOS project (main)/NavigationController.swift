//
//  NavigationController.swift
//  IOS project (main)
//
//  Created by Martin Malmström on 2018-11-30.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor.MilkshakerColors.darkPink
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

}
