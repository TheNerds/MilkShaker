

import UIKit

class UpgradesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let sections = ["Store","Upgrades"]
    
    //change sizes, font and colors of section headers
    var headerSectionHeight: CGFloat = 60
    var headerSectionTextSize = 60
    var headerSectionFont = UIFont(name: "Noteworthy-Light", size: 40)
    var headerSectionTextColor = UIColor.MilkshakerColors.lightPink
    var headerSectionColor = UIColor.MilkshakerColors.blue
    var currentCurrencyFont = UIFont(name: "Noteworthy-Light", size: 15)
    
    
    var upgrades: [Upgrade] = []
    var store: [Upgrade] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upgrades = Milkshaker.instance.allUpgrades
        store = Milkshaker.instance.allUpgrades
        store.remove(at: 0)
        let dummyViewHeight = CGFloat(50)
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: dummyViewHeight))
        self.tableView.contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: Reloads the Table view when something is bought
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
}

// MARK: Functions for the tableCells.
extension UpgradesViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: returns how many rows is going to be used
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (store.count)
        }else{
            return (upgrades.count)
        }
    }
    
    // MARK: Returns how many sections is going to be used
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    // MARK: When rendering the in the upgradeViewController
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "upgradeCell", for: indexPath) as? UpgradesTableViewCell{
            
            switch indexPath.section {
            case 0:
                cell.upgradeImage.image = UIImage(named: store[indexPath.row].imageName)
                cell.upgradeTitleLabel.text = store[indexPath.row].name
                
                if store[indexPath.row].litresPerSecond < 10{
                    cell.upgradeInfoLabel.text = "Produces " + String(format: "%.1f", store[indexPath.row].litresPerSecond) + " litres/second"
                }
                else{
                    cell.upgradeInfoLabel.text = "Produces " + String(format: "%.0f", store[indexPath.row].litresPerSecond) + " litres/second"
                }
                
                cell.upgradePriceLabel.text = "Buy 1 " + store[indexPath.row].name + " for " + suffixNumber(store[indexPath.row].cost) + " litres"
                cell.upgradeAmountLabel.text = "Amount: " + String(store[indexPath.row].numberOf)
                cell.backgroundColor = UIColor.clear
                return cell
            case 1:
                cell.upgradeImage.image = UIImage(named: upgrades[indexPath.row].imageName)
                cell.upgradeTitleLabel.text = upgrades[indexPath.row].name
                cell.upgradeInfoLabel.text = "Make all 20% more efficient"
                cell.upgradePriceLabel.text = "Upgrade all for " + suffixNumber(upgrades[indexPath.row].upgradeCost) + " litres"
                cell.upgradeAmountLabel.text = "Level: " + String(upgrades[indexPath.row].level)
                cell.backgroundColor = UIColor.clear
                return cell
            default:
                return cell
            }
        }else{
            return UITableViewCell()
        }
    }
    
    // MARK: To change to a custom height for every cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;//Choose your custom row height
    }
    
    // MARK: Returns how many sections the table should have
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerSectionHeight
    }
    
    //MARK: - Custom header
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    // MARK: Currency counter at header in tableView
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var currentCurrency: String
        if Milkshaker.instance.currency >= 0.1 {
            currentCurrency = suffixNumber(Milkshaker.instance.currency)
        } else {
            currentCurrency = String(Milkshaker.instance.currency)
        }
        
        
        let headerSections = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: headerSectionHeight))
        headerSections.backgroundColor = headerSectionColor
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: headerSections.frame.width - 16, height: headerSectionHeight))
        headerLabel.text = self.sections[section]
        headerLabel.textColor = headerSectionTextColor
        headerLabel.textAlignment = .center
        headerLabel.font = headerSectionFont
        
        let currentCurrencyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: headerSections.frame.width - 16, height: headerSectionHeight))
        currentCurrencyLabel.text = currentCurrency + " L"
        currentCurrencyLabel.textAlignment = .right
        currentCurrencyLabel.font = currentCurrencyFont
        
        
        headerSections.addSubview(currentCurrencyLabel)
        headerSections.addSubview(headerLabel)
        return headerSections
    }
    
    // MARK: When a cell is pressed
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            Milkshaker.instance.allUpgrades[indexPath.row+1].buy()
            self.tableView.reloadData()
        case 1:
            if indexPath.row == 0
            {
                Milkshaker.instance.allUpgrades[indexPath.row].upgradeShaker()
                self.tableView.reloadData()
            }else{
                Milkshaker.instance.allUpgrades[indexPath.row].upgrade()
                self.tableView.reloadData()
            }
            
       default:
            break
        }
    }
    
    //MARK: - change suffix, 1000 to 1k etc. also changes number of decimals
    func suffixNumber(_ number: Double) -> String{
        if number < 1000{
            return String(truncate(number: number, numberOfDecimals: 1))
        }else{
            let sign = ((number < 0) ? "-" : "");
            let exp:Int = Int(log10(number)/3.0)
            if exp == 0{
                return "\(number)"
            }
            let units:[String] = ["K","M","B","T"]
            let roundedNum:Double = round(10*number/pow(1000.0,Double(exp)))/10
            return "\(sign)\(roundedNum)\(units[exp-1])"
        }
    }
    func truncate(number: Double, numberOfDecimals: Double) -> Double{
        let factor : Double = pow(Double(10), numberOfDecimals)
        return (Double(Int((number)*factor))/factor)
    }
    
    
    
    
}
