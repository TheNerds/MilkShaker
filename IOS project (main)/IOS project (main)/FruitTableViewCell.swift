//
//  FruitTableViewCell.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-27.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class FruitTableViewCell: UITableViewCell {

    @IBOutlet weak var fruitNameLabel: UILabel!
    @IBOutlet weak var favoriteLabel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
