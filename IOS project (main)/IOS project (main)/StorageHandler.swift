//
//  StorageHandler.swift
//  APIDemo
//
//  Created by Ebbas on 2018-11-22.
//  Copyright © 2018 enappstudio. All rights reserved.
//

import Foundation

class Storagehandler {
    
    static func getFavorite() -> String {
        return UserDefaults.standard.string(forKey: "favorite") ?? ""
    }
    
    static func setFavorite(_ favorite: String) {
        UserDefaults.standard.set(favorite, forKey: "favorite")
    }
}
