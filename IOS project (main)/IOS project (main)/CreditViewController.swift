//
//  CreditViewController.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-05.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class CreditViewController: UIViewController {
    
    @IBOutlet weak var creditTableView: UITableView!
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var catLabel: UILabel!
    
    var creators: [Creator] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creators = Milkshaker.instance.Creators
        //Calling the function for our API to spit
        //The random quote
        fetchJSON()
        
    }
    // MARK: Retrieves the API and updates labels in creditViewController
    func fetchJSON() {
        //The url for the API
        let urlString = "https://talaikis.com/api/quotes/random/"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, err) in
            DispatchQueue.main.async {
                if let err = err {
                    print("Failed to get data from url:", err)
                    return
                }
                guard let data = data else { return }
                do {
                    let qoute = try JSONDecoder().decode(Quote.self, from: data)
                    self.authorLabel.text = qoute.author
                    self.quoteLabel.text = qoute.quote
                    self.catLabel.text = qoute.cat
                } catch let jsonErr {
                    print("Failed to decode:", jsonErr)
                }
            }
        }.resume()
    }
    
    

}

// MARK: TableView settings for the credit Table
extension CreditViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: Returns row size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creators.count
    }
    
    // MARK: Returns section size
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: Returns cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "creatorCell",for: indexPath) as? CreatorTableViewCell {
            let creator = creators[indexPath.row]
            cell.nameLabel.text = creator.name
            cell.backgroundColor = UIColor.clear
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    // MARK: Returns a custom cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;//Choose your custom row height
    }
    
    
}

