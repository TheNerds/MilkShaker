//
//  ViewController.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-10-23.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var autoCurrencyLabel: UILabel!
    @IBOutlet weak var milkshakeButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.becomeFirstResponder() // To get shake gesture
        CoreDataManager.instance.loadDataLocally()
        for i in 0..<Milkshaker.instance.allUpgrades.count{
           CoreDataManager.instance.loadUpgradeDataFor(index: i)
        }
        PushNotificationManager.instance.sendNotification(title: "Milkshaker", body: " Hello, we miss you!")
        
        updateFlavorImage()
        updateCurrencyTimeInterval()
    }
    override func viewDidAppear(_ animated: Bool) {
        updateUI()
        updateFlavorImage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        CoreDataManager.instance.storeDataLocally()
        CoreDataManager.instance.storeUpgradeDataLocally()
    }
    // MARK: - Timer
    // adds currency each second
    func updateCurrencyTimeInterval(){
        Timer.scheduledTimer(timeInterval: 1/Milkshaker.instance.updatesPerSecond, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    @objc func updateTimer(){
        Milkshaker.instance.addAutoCurrency()
        updateUI()
    }
    // MARK: - Shake
    // We are willing to become first responder to get shake motion
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            Milkshaker.instance.shake()
            currencyLabel.text = String(Milkshaker.instance.currency)
            
            animateMilkshake()
            motionEnded(motion, with: event)
        }
    }
    // Enable detection of shake motion
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
    }
    //MARK: - milkshake pressed
    @IBAction func milkshakePressed(_ sender: UIButton) {
        Milkshaker.instance.click()
        currencyLabel.text = String(Milkshaker.instance.currency)
        
        animateMilkshake()
        
    }
    // MARK: - animate
    //animates milkshake button to change size and back to original size after clicked
    //if milkshake is pressed/shaked before animation is complete it will stop the animation and start it again
    func animateMilkshake(){
        soundManager.instance.playSound(soundFile: "milkshaker_Sound")
        self.milkshakeButton.layoutIfNeeded()
        self.milkshakeButton.updateConstraintsIfNeeded()
        self.milkshakeButton.setNeedsUpdateConstraints()
        
        self.milkshakeButton.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.1,
        animations: {
            self.milkshakeButton.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.1){
                self.milkshakeButton.transform = CGAffineTransform.identity
            }
        })
    }
    //MARK: - updates ui
    func updateUI(){
        if(Milkshaker.instance.currency < 10){
            currencyLabel.text = String(truncate(number: Milkshaker.instance.currency, numberOfDecimals: 2))
        }
        else if(Milkshaker.instance.currency < 100){
            currencyLabel.text = String(truncate(number: Milkshaker.instance.currency, numberOfDecimals: 1))
        }
        else if(Milkshaker.instance.currency > 1000){
            currencyLabel.text = suffixNumber(number: Milkshaker.instance.currency)
        }
        else{
            currencyLabel.text = String(Int(Milkshaker.instance.currency))
        }
        
        //print(Milkshaker.instance.allUpgrades[1].litresPerSecond)
        
        if(Milkshaker.instance.autoCurrencyPerSec < 10){
            autoCurrencyLabel.text = String(truncate(number: Milkshaker.instance.autoCurrencyPerSec, numberOfDecimals: 2))
        }
        else if(Milkshaker.instance.autoCurrencyPerSec < 100){
            autoCurrencyLabel.text = String(truncate(number: Milkshaker.instance.autoCurrencyPerSec, numberOfDecimals: 1))
        }
        else if(Milkshaker.instance.autoCurrencyPerSec > 1000){
            autoCurrencyLabel.text = suffixNumber(number: Milkshaker.instance.autoCurrencyPerSec)
        }
        else{
            autoCurrencyLabel.text = String(Int(Milkshaker.instance.autoCurrencyPerSec))
        }
    }
    func updateFlavorImage(){
        let favorite = Storagehandler.getFavorite()
        if favorite != "" {
            milkshakeButton.setImage(UIImage(named: favorite), for: .normal)
        }
    }
    // MARK: - Change number of decimals / K for 1000 etc..
    func suffixNumber(number: Double) -> String{
        let sign = ((number < 0) ? "-" : "");
        let exp:Int = Int(log10(number)/3.0)
        let units:[String] = ["K","M","B","T","q","Q","s","S","O","N","D","C"]
        let roundedNum:Double = round(10*number/pow(1000.0,Double(exp)))/10
        return "\(sign)\(roundedNum)\(units[exp-1])"
    }
    func truncate(number: Double, numberOfDecimals: Double) -> Double{
        let factor : Double = pow(Double(10), numberOfDecimals)
        return (Double(Int((number)*factor))/factor)
    }
    
    // MARK: Store and load data.
    func storeDataLocally(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let milkshaker = NSEntityDescription.insertNewObject(forEntityName: "MilkshakerModel", into: context)
        
        milkshaker.setValue(Milkshaker.instance.currency, forKey: "currency")
        milkshaker.setValue(Milkshaker.instance.multiplier, forKey: "multiplier")
        
        print(Milkshaker.instance.currency)
        print(Milkshaker.instance.multiplier)
        
        
        do {
            try context.save()
            print("Saved")
        } catch{
            // Catch dis shit
        }
    }
    
    // MARK: Loads data from previous save
    func loadDataLocally() -> (Double, Double) {
        var realCurrency : Double = 0
        var realMultiplier: Double = 0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MilkshakerModel")
        
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let currency = result.value(forKey: "currency") as? Double{
                        print(currency)
                        realCurrency = currency
                    }
                    if let multiplier = result.value(forKey: "multiplier") as? Double{
                        print(multiplier)
                        if multiplier <= 0{
                            realMultiplier = 1
                        } else {
                                realMultiplier = multiplier
                        }
                    }
                }
            }
        } catch {
            // Catch dis shit
        }
        //return(0,1)
        return (realCurrency, realMultiplier)
    }
}


