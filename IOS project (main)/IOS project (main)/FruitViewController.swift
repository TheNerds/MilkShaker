//
//  FruitViewController.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-27.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class FruitViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var Fruits: [Fruit] = []
    var orderedFruits: [String] = []
    var remove: [String] = ["Aktualisiertes Produkt", "Apples", "Apple", "Dragon Fruit", "Dried Pineapples", "Figs", "Kaki", "Lychee", "Mango fresh", "Neues Produkt", "Pecan","Coconut", "Produkt zum Aktualisieren", "Rambutan","Cherries","Lemon" ,"Test"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        
        RestHandler.getFruits{ (success) in
            self.Fruits = DataHandler.instance.Fruit
            for x in self.Fruits{
                if !self.orderedFruits.contains(x.name) && !self.remove.contains(x.name){
                    if(x.name == "Watnuts"){
                        self.orderedFruits.append("Walnuts")
                    }
                    else{
                        self.orderedFruits.append(x.name)
                    }
                    
                }
            }
            for x in self.orderedFruits{
                print(x)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }
}

// MARK: TableView settings for the credit Table
extension FruitViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: Returns row size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderedFruits.count
    }
    
    // MARK: Returns section size
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: Returns cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "fruitCell",for: indexPath) as? FruitTableViewCell {
            let fruit = orderedFruits[indexPath.row]
            cell.fruitNameLabel.text = fruit
            
            if DataHandler.instance.isFavorite(fruit) {
                cell.favoriteLabel.image = UIImage(named: "favorite")
                
            } else {
                cell.favoriteLabel.image = UIImage(named: "no_favorite")
            }
            cell.backgroundColor = UIColor.clear
            return cell
        } else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fruit = orderedFruits[indexPath.row]
        DataHandler.instance.setFavorite(fruit)
        if let visibleIndexPaths = tableView.indexPathsForVisibleRows {
            tableView.reloadRows(at: visibleIndexPaths, with: .automatic)
        }
    }
    
}
