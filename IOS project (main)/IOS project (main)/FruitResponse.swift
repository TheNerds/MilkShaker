//
//  PeopleResponse.swift
//  APIDemo
//
//  Created by Ebbas on 2018-11-22.
//  Copyright © 2018 enappstudio. All rights reserved.
//

import Foundation

struct FruitResponse: Codable {
    var products: [Fruit]
}
