//
//  SettingViewController.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-02.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit
import AVFoundation

class SettingViewController: UIViewController{
    
    

    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var settingsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        muteButton.setImage(UIImage(named: "Volume"), for: .normal)
        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: Resetting the game to default values
    @IBAction func resetProgress(_ sender: UIButton){
        // create the alert
        let alert = UIAlertController(title: "Resetting", message: "Are you sure you want to reset all your progress?", preferredStyle: UIAlertController.Style.alert)
        // add actions (buttons)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { action in
                    CoreDataManager.instance.resetData()
                    CoreDataManager.instance.loadDataLocally()
                    for i in 0..<Milkshaker.instance.allUpgrades.count{
                        CoreDataManager.instance.loadUpgradeDataFor(index: i)
                    }
                    
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    // MARK: Muting the sound for the entire app
    @IBAction func SoundButton(_ sender: UIButton) {
        if soundManager.instance.isMute(){
            soundManager.instance.unMuteSound()
            self.muteButton.setImage(UIImage(named: "Volume"), for: .normal)
            
        } else {
            soundManager.instance.muteSound()
            self.muteButton.setImage(UIImage(named: "Volume_off"), for: .normal)
        }
    }
}



