//
//  Quote.swift
//  IOS project (main)
//
//  Created by Anton Andrésen on 2018-11-09.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation

struct Quote: Decodable {
    let author : String
    let quote : String
    let cat : String
}
