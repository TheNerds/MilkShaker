//
//  Fruit.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-27.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation

class Fruit: Codable {
    
    var name: String
    
    private enum CodingKeys: String, CodingKey {
        case name
    }
    
    init(name: String = "") {
        self.name = name
    }
}
