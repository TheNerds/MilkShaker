//
//  Upgrade.swift
//  IOS project (main)
//
//  Created by Anton Andrésen on 2018-11-05.
//  Copyright © 2018 Nerds INC. All rights reserved.
//



import Foundation

class Upgrade{
    var name: String
    var cost: Double
    var numberOf : Int
    var litresPerSecond : Double
    var level : Int
    var upgradeCost : Double
    var imageName : String
    
    init(name: String, cost: Double, numberOf : Int, litresPerSecond : Double, level : Int, upgradeCost : Double, imageName : String){
        self.name = name
        self.cost = cost
        self.numberOf = numberOf
        self.litresPerSecond = litresPerSecond
        self.level = level
        self.upgradeCost = upgradeCost
        self.imageName = imageName
        //yes
    }
    //buy "auto currency"
    func buy(){
        if Milkshaker.instance.currency >= cost{
            Milkshaker.instance.takeMoney(amount: cost)
            self.numberOf += 1
            self.cost = cost*1.2
        }else {
            return
        }
        
    }
    //upgrade "auto currency"
    func upgrade(){
        if Milkshaker.instance.currency >= upgradeCost{
            Milkshaker.instance.takeMoney(amount: upgradeCost)
            self.level += 1
            self.upgradeCost = upgradeCost*1.2
            self.litresPerSecond *= 1.2
        }else{
            return
        }
    }
    func upgradeShaker(){
        if Milkshaker.instance.currency >= upgradeCost{
            Milkshaker.instance.takeMoney(amount: upgradeCost)
            self.upgradeCost = upgradeCost*1.2
            self.level += 1
            Milkshaker.instance.multiplier *= 1.2
        }
    }
    
}
