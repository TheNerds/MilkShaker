//
//  Milkshaker.swift
//  IOS project (main)
//
//  Created by Anton Andrésen on 2018-10-26.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation

class Milkshaker {
    
    static let instance = Milkshaker()
    
    var allUpgrades : [Upgrade] = []
    
    var Creators : [Creator] = []
    var currency : Double
    var multiplier : Double
    var autoCurrencyPerSec : Double
    
    var updatesPerSecond : Double = 60
    
    let defaultCost : [Double] = [0.0, 10.0, 100.0, 500.0]
    let defaultLitresPerSecond : [Double] = [0,0.1,1.5,8]
    let defaultUpgradeCost : [Double] = [5.0, 100.0, 500.0, 2500.0]
    
    
    init() {
        self.currency = 0
        self.multiplier = 1
        self.autoCurrencyPerSec = 0
        
        self.allUpgrades = [
            Upgrade(name: "Milkshaker", cost: defaultCost[0], numberOf: 0, litresPerSecond: defaultLitresPerSecond[0], level: 1, upgradeCost: defaultUpgradeCost[0], imageName: "Milkshaker_Milkshake"),
            Upgrade(name: "Cow", cost: defaultCost[1], numberOf: 0, litresPerSecond: defaultLitresPerSecond[1], level: 1, upgradeCost: defaultUpgradeCost[1], imageName: "cow"),
            Upgrade(name: "Milk Truck", cost: defaultCost[2], numberOf: 0, litresPerSecond: defaultLitresPerSecond[2], level: 1, upgradeCost: defaultUpgradeCost[2], imageName: "milktruck"),
            Upgrade(name: "Factory", cost: defaultCost[3], numberOf: 0, litresPerSecond: defaultLitresPerSecond[3], level: 1, upgradeCost: defaultUpgradeCost[3], imageName: "factory")
        ]
        self.Creators = [
            Creator(name: "Viktor Zenk", age: 20, info: "\"He's a cool dude!\" - Albert Einstein"),
            Creator(name: "Adam Håkansson",age: 21,info: "The guy that works with all the gitlab stuff xD"),
            Creator(name: "Anton Andrésen", age: 20, info: "\"Unity is better\" - Anton"),
            Creator(name: "Martin Malmström", age: 27, info: "Danskjävel")
        ]
    }
    func takeMoney(amount : Double){
        currency -= amount
    }
    func shake()  {
        currency += 0.2 * multiplier
    }
    func click(){
        currency += 0.1 * multiplier
    }
    func addAutoCurrency(){
        autoCurrencyPerSec = 0
        for upgrade in allUpgrades{
            autoCurrencyPerSec += upgrade.litresPerSecond * Double(upgrade.numberOf)
        }
        currency += (autoCurrencyPerSec/updatesPerSecond)
    }
}
