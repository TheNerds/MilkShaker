//
//  PushNotificationManager.swift
//  IOS project (main)
//
//  Created by Anton Andrésen on 2018-11-26.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation
import UserNotifications

class PushNotificationManager {
    
    public static let instance = PushNotificationManager()
    
    let content = UNMutableNotificationContent()
    
    func sendNotification(title: String, body: String) {
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        
        /*let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)*/
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        
        dateComponents.hour = 18
        //dateComponents.minute = 14
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier: "testID", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
    }
}
