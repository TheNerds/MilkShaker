//
//  Sound.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-13.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import Foundation
import AVFoundation

class soundManager{
    static let instance = soundManager()
    
    var sound:AVAudioPlayer = AVAudioPlayer()
    var soundFile: String
    var volume: Float
    var mute: Bool
    init(){
        self.soundFile = ""
        self.volume = 1
        self.mute = false
        do {
            let path = Bundle.main.path(forResource: self.soundFile, ofType:"mp3")!
            let url = URL(fileURLWithPath: path)
            self.sound = try AVAudioPlayer(contentsOf: url)
        } catch {
            print("Something went wrong when playing the sound!")
        }
    }
    func playSound(soundFile:String){
        self.soundFile = soundFile
        let path = Bundle.main.path(forResource: self.soundFile, ofType:"mp3")!
        let url = URL(fileURLWithPath: path)
        do {
            self.sound = try AVAudioPlayer(contentsOf: url)
            updateVolume()
            self.sound.play()
        } catch {
            print("Something went wrong when playing the sound!")
        }
    }
    func updateVolume(){
        sound.volume = self.volume
    }
    func muteSound(){
        self.volume = 0
        self.sound.volume = self.volume
        print(sound.volume)
        self.mute = true
    }
    func unMuteSound(){
        self.volume = 1
        self.sound.volume = self.volume
        print(sound.volume)
        self.mute = false
    }
    func isMute() -> Bool {  return mute  }
}
