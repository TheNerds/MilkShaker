//
//  MainUITabBarController.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-11-02.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class MainUITabBarController: UITabBarController {

    @IBOutlet var GameViewTab: UITabBar!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: Tab bar colors
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBar.barTintColor = UIColor.MilkshakerColors.blue
        self.tabBar.tintColor = UIColor.MilkshakerColors.darkPink
        self.tabBar.unselectedItemTintColor = UIColor.MilkshakerColors.lightPink
    }

}
