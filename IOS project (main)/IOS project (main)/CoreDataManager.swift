//
//  SaveData.swift
//  IOS project (main)
//
//  Created by Adam Håkansson on 2018-10-26.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit
import CoreData

//going to be used to save data inbetween screen changes
//not used right now!

class CoreDataManager {
    
    static let instance = CoreDataManager()
    
    //MARK: - Store data
    func storeDataLocally(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let milkshaker = NSEntityDescription.insertNewObject(forEntityName: "MilkshakerModel", into: context)
        
        milkshaker.setValue(Milkshaker.instance.currency, forKey: "currency")
        milkshaker.setValue(Milkshaker.instance.multiplier, forKey: "currencyMultiplier")
        milkshaker.setValue(Milkshaker.instance.autoCurrencyPerSec, forKey: "currencyPerSec")
        
        print(Milkshaker.instance.currency, Milkshaker.instance.multiplier)
        
        do {
            try context.save()
            print("Saved")
        } catch{
            // Catch dis shit
        }
    }
    func storeUpgradeDataLocally(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entities : [NSManagedObject] = [
            NSEntityDescription.insertNewObject(forEntityName: "MilkshakeUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "CowUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "TruckUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "FactoryUpgradeModel", into: context)
        ]
        for i in 0..<entities.count {
            entities[i].setValue(Milkshaker.instance.allUpgrades[i].cost, forKey: "cost")
            entities[i].setValue(Milkshaker.instance.allUpgrades[i].numberOf, forKey: "numberOf")
            print(Milkshaker.instance.allUpgrades[i].numberOf)
            entities[i].setValue(Milkshaker.instance.allUpgrades[i].litresPerSecond, forKey: "litresPerSecond")
            print(Milkshaker.instance.allUpgrades[i].litresPerSecond)
            entities[i].setValue(Milkshaker.instance.allUpgrades[i].upgradeCost, forKey: "upgradeCost")
            entities[i].setValue(Milkshaker.instance.allUpgrades[i].level, forKey: "level")
        }
        do {
            try context.save()
            print("Upgrades saved")
        } catch{
            // Catch dis shit
        }
    }
    //MARK: - Load data
    func loadDataLocally(){
        var realCurrency : Double = 0
        var realCurrencyMultiplier: Double = 1
        var realCurrencyPerSec : Double = 0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MilkshakerModel")
        
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let currency = result.value(forKey: "currency") as? Double{
                        realCurrency = currency
                    }
                    if let currencyMultiplier = result.value(forKey: "currencyMultiplier") as? Double{
                        if currencyMultiplier < 1{
                            realCurrencyMultiplier = 1
                        } else {
                            realCurrencyMultiplier = currencyMultiplier
                        }
                    }
                    if let currencyPerSec = result.value(forKey: "currencyPerSec") as? Double{
                        realCurrencyPerSec = currencyPerSec
                    }
                    
                }
            }
        } catch {
            // Catch dis shit
        }
        print(realCurrency)
        print(realCurrencyMultiplier)
        print(realCurrencyPerSec)
        (Milkshaker.instance.currency, Milkshaker.instance.multiplier, Milkshaker.instance.autoCurrencyPerSec) =  (realCurrency, realCurrencyMultiplier, realCurrencyPerSec)
    }
    func loadUpgradeDataFor(index : Int){
        var tableData: (Double, Int, Double, Double, Int) = (Milkshaker.instance.defaultCost[index], 0, Milkshaker.instance.defaultLitresPerSecond[index], Milkshaker.instance.defaultUpgradeCost[index], 1)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entityNames : [String] = ["MilkshakeUpgradeModel", "CowUpgradeModel", "TruckUpgradeModel", "FactoryUpgradeModel"]
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityNames[index])
        
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                let result = results[results.count-1] as! NSManagedObject
                
                if let cost = result.value(forKey: "cost") as? Double{
                    tableData.0 = cost
                }
                if let numberOf = result.value(forKey: "numberOf") as? Int{
                    tableData.1 = numberOf
                }
                if let litresPerSecond = result.value(forKey: "litresPerSecond") as? Double{
                    tableData.2 = litresPerSecond
                }
                if let upgradeCost = result.value(forKey: "upgradeCost") as? Double{
                    tableData.3 = upgradeCost
                }
                if let level = result.value(forKey: "level") as? Int{
                    tableData.4 = level
                }
            }
        } catch {
            // Catch dis shit
        }
        (Milkshaker.instance.allUpgrades[index].cost,Milkshaker.instance.allUpgrades[index].numberOf,Milkshaker.instance.allUpgrades[index].litresPerSecond,Milkshaker.instance.allUpgrades[index].upgradeCost,Milkshaker.instance.allUpgrades[index].level) = tableData
    }
    
    func loadUpgradeDataLocally() -> [(Double, Int, Double, Double, Int)]{
        let defaultUpgrade = (0.0,0,0.0,0.0,1)
        var tableData: [(Double, Int, Double, Double, Int)] = [defaultUpgrade, defaultUpgrade, defaultUpgrade, defaultUpgrade]
        var requestResults: [[Any]] = []
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let requests: [NSFetchRequest<NSFetchRequestResult>] = [
            NSFetchRequest<NSFetchRequestResult>(entityName: "MilkshakeUpgradeModel"),
            NSFetchRequest<NSFetchRequestResult>(entityName: "CowUpgradeModel"),
            NSFetchRequest<NSFetchRequestResult>(entityName: "TruckUpgradeModel"),
            NSFetchRequest<NSFetchRequestResult>(entityName: "FactoryUpgradeModel")
        ]
        
        for request in requests{
            request.returnsObjectsAsFaults = false
        }
        
        do{
            for request in requests{
                try requestResults.append(context.fetch(request))
            }
            
            for result in requestResults{
                if result.count > 0{
                    var i = 0
                    for singleResult in result as! [NSManagedObject]{
                        if let cost = singleResult.value(forKey: "cost") as? Double{
                            tableData[i].0 = cost
                        }
                        if let numberOf = singleResult.value(forKey: "numberOf") as? Int{
                            tableData[i].1 = numberOf
                        }
                        if let litresPerSecond = singleResult.value(forKey: "litresPerSecond") as? Double{
                            tableData[i].2 = litresPerSecond
                        }
                        if let upgradeCost = singleResult.value(forKey: "upgradeCost") as? Double{
                            tableData[i].3 = upgradeCost
                        }
                        if let level = singleResult.value(forKey: "level") as? Int{
                            tableData[i].4 = level
                        }
                        i += 1
                    }
                }
            }

        } catch {
            // Catch dis shit
        }
        print(tableData)
        return tableData
    }
    //MARK: - Reset data
    func resetData(){
        let defaultUpgrade = (0.0,0,0.0,0.0,1)
        let defaults = (0,1,0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        // Currency related stats reset.
        let milkshaker = NSEntityDescription.insertNewObject(forEntityName: "MilkshakerModel", into: context)
        
        milkshaker.setValue(defaults.0, forKey: "currency")
        milkshaker.setValue(defaults.1, forKey: "currencyMultiplier")
        milkshaker.setValue(defaults.2, forKey: "currencyPerSec")
        
        // Upgrade related stats reset.
        let entities : [NSManagedObject] = [
            NSEntityDescription.insertNewObject(forEntityName: "MilkshakeUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "CowUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "TruckUpgradeModel", into: context),
            NSEntityDescription.insertNewObject(forEntityName: "FactoryUpgradeModel", into: context)
        ]
        for i in 0..<entities.count {
            entities[i].setValue(Milkshaker.instance.defaultCost[i], forKey: "cost")
            entities[i].setValue(defaultUpgrade.1, forKey: "numberOf")
            entities[i].setValue(Milkshaker.instance.defaultLitresPerSecond[i], forKey: "litresPerSecond")
            entities[i].setValue(Milkshaker.instance.defaultUpgradeCost[i], forKey: "upgradeCost")
            entities[i].setValue(defaultUpgrade.4, forKey: "level")
        }
        do {
            try context.save()
            print("DB Reset")
        } catch{
            // Catch dis shit
        }
    }
 
    
}
