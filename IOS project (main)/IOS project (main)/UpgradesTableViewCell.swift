//
//  UpgradesTableViewCell.swift
//  IOS project (main)
//
//  Created by Rebecca Lundqvist  on 2018-11-05.
//  Copyright © 2018 Nerds INC. All rights reserved.
//

import UIKit

class UpgradesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var upgradeImage: UIImageView!
    @IBOutlet weak var upgradeTitleLabel: UILabel!
    @IBOutlet weak var upgradeInfoLabel: UILabel!
    @IBOutlet weak var upgradePriceLabel: UILabel!
    @IBOutlet weak var upgradeAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
