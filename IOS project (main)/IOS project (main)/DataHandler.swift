//
//  DataHandler.swift
//  APIDemo
//
//  Created by Ebbas on 2018-11-22.
//  Copyright © 2018 enappstudio. All rights reserved.
//

import Foundation

class DataHandler {
    static let instance = DataHandler()
    
    var Fruit: [Fruit] = []
    
    private var favoriteFruit: String?
    
    func isFavorite(_ favorite: String) -> Bool {
        if favoriteFruit == nil {
            favoriteFruit = Storagehandler.getFavorite()
        }
        return favoriteFruit! == favorite
    }
    func setFavorite(_ favorite: String) {
        favoriteFruit = favorite
        Storagehandler.setFavorite(favorite)
    }
}
